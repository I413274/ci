package com.example.dep;

/**
 * Hello name!
 *
 */
public class Dep
{
    public static String hello( String name )
    {
        return ( "Hello " + name + "!" );
    }
}