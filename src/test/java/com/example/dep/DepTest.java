package com.example.dep;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

/**
 * Unit test for simple Dep.
 */
public class DepTest 

{

    /**
     * Rigourous Test  :-)
     */
    @Test
    public void testDep()
    {
        String toCompare = Dep.hello("Joe");
        assertEquals("Hello Joe!", toCompare);
    }
}
